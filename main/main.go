package main

import (
	"log"
	"net/http"
	"bitbucket.org/svrsilicium/systemA/models"
)

func main() {

	router := models.NewRouter()

	log.Println("listening systemA on 8080")

	models.StartConductor()

	log.Fatal(http.ListenAndServe(":8080", router))
}
