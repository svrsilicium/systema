package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"
	"crypto/sha1"
	"encoding/hex"

	"github.com/gorilla/mux"

	"bitbucket.org/svrsilicium/systemA/conf"
)

// -- Products list
var cars []*Car = Cars{
	&Car{Id: 1, Name: "car 1"},
	&Car{Id: 2, Name: "car 2"},
	&Car{Id: 3, Name: "car 3"},
}

// -- channel for reservations
var Jobs chan *Reservation

func Index(w http.ResponseWriter, r *http.Request) {
	/*
	Index page, just for welcoming
	 */
	fmt.Fprintln(w, "Welcome to systemA!")
}

func CarIndex(w http.ResponseWriter, r *http.Request) {
	/*
	Returns list of products (cars) that are not reserved
	 */
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var responseCars Cars

	for _, car := range cars {
		if !car.Reserved {
			responseCars = append(responseCars, car)
		}
	}

	if err := json.NewEncoder(w).Encode(responseCars); err != nil {
		panic(err)
	}
}


func CarReserve(w http.ResponseWriter, r *http.Request) {
	/*
	Reserves product by its id
	Returns reservation hash or message, that product is already reserved
	 */
	vars := mux.Vars(r)
	carId := vars["carId"]
	var hash string
	var found bool

	for _, car := range cars {
		if strconv.Itoa(car.Id) == carId && !car.Reserved  {
			found = true
			car.Reserved = true
			car.Due = time.Now().Local().Add(time.Second * time.Duration(conf.ReservationTime))

			hash = getHash(carId + car.Due.String())
			//fmt.Printf("Generated hash %T: %s\n", hash, hash)

			die := make(chan bool)
			resrv := Reservation{car, die}
			Reservations[resrv] = hash

			// -- pass hash to System B
			fmt.Fprintln(w, hash)

			Jobs <- &resrv
			break
		} else if strconv.Itoa(car.Id) == carId && car.Reserved {
			found = true
			// -- maybe, it is better to return some error code
			fmt.Fprintf(w, "Car %s is already Reserved", carId)
		}
	}
	if !found {
		// -- not a code, but for now is ok, I think
		// -- todo: return code, or 404 page
		fmt.Fprintln(w,"404 not found")
	}
}

func getHash(str string) string {
	h := sha1.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}


func CarConfirm(w http.ResponseWriter, r *http.Request) {
	var hashBody HashBody
	if r.Body == nil {
		http.Error(w, "Request body is empty", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&hashBody)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	//fmt.Printf("came [%T] hash: [%s]\n", hashBody.Hash, hashBody.Hash)

	if KillWorker(string(hashBody.Hash)) {
		fmt.Println("Reservation confirmed, worker killed")
	} else {
		fmt.Println("error handling")
	}

	fmt.Fprintf(w, "OK, confirmed on hash %s\n", hashBody.Hash)

	// -- Prints request body
	//requestDump, err1 := httputil.DumpRequest(r, true)
	//if err1 != nil {
	//	fmt.Println(err1)
	//}
	//fmt.Println(string(requestDump))
}

func KillWorker(hash string) bool {
	/*
	Kills worker if confirmation command has come
	It could be better, now it has some hardcode and bad practices
	 */
	//fmt.Println("*** came to kill ", hash)
	for reservation, hsh := range Reservations {
		if hsh == hash {
			reservation.Die <- true
			delete(Reservations, reservation)
			ConfirmedList = append(
				ConfirmedList,
				&ConfirmedCar{
					reservation.Product.Id,
					reservation.Product.Name,
					"user0343"})
			return true
		}
	}
	return false

	//_, ok := Reservations[hash]
	//if ok {
	//	delete(Reservations, hash)
	//}
}




func CarShow(w http.ResponseWriter, r *http.Request) {
	/*
	Shows product info by its id
	This function is not required by the task, just for me
	 */
	vars := mux.Vars(r)
	carId := vars["carId"]
	requestedCar := &Car{}

	for _, car := range cars {
		if strconv.Itoa(car.Id) == carId{
			requestedCar = car
			break
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(requestedCar); err != nil {
		panic(err)
	}
}