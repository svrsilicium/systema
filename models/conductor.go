package models

import (
	"fmt"
	"time"
	"bitbucket.org/svrsilicium/systemA/conf"
)

func StartConductor() {
	/*
	Init channel and lists, starting conductor
	 */
	Jobs = make(chan *Reservation, 0)
	Reservations = make(map[Reservation]string)
	ConfirmedList = make([]*ConfirmedCar, 0)

	go conductor(0, Jobs)
}

func conductor(id int, jobs <-chan *Reservation) {
	/*
	Waits for jobs,
	launches a worker for each job, passing id, job and die channel
	 */
	for job := range jobs {
		fmt.Printf("conductor %d got a car %T with id %d\n", id, job, job.Product.Id)
		go worker(job.Product.Id, job, job.Die)
	}
}

func worker(id int, job *Reservation, die chan bool) {
	/*
	Checks if reservation is out of time
	Stops on command from die channel
	 */
	fmt.Printf("worker %d got a car %T with id %d\n", id, job.Product, job.Product.Id)
	for {
		select {
		case <-die:
			die <- true
			return
		default:
			timeNow := time.Now()
			fmt.Printf("[worker %d] now: %s\n", id, timeNow.String())
			fmt.Printf("[worker %d] car: %s\n", id, job.Product.Due.String())

			if job.Product.Due.After(timeNow) && job.Product.Reserved {
				// -- still reserved and time is not out
				fmt.Printf("[worker %d] waiting %d secs\n", id, conf.TimeToWait)
				time.Sleep(time.Second * time.Duration(conf.TimeToWait))
				continue
			} else if timeNow.After(job.Product.Due) && job.Product.Reserved {
				// -- time is out but car reserved - decline reservation
				job.Product.Reserved = false
				job.Product.Due = time.Time{}
				// todo delete from reserved
				fmt.Printf("[worker %d] time is out, deleting reservation\n", id)
				return
			}
		}
	}
}