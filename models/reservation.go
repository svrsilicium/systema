package models

type Reservation struct {
	Product		*Car
	Die			chan bool
}

var Reservations map[Reservation]string

type Hash []byte

