package models

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"CarIndex",
		"GET",
		"/cars",
		CarIndex,
	},
	Route{
		"CarReserve",
		"GET",
		"/car/{carId}",
		CarReserve,
	},
	Route{
		"CarConfirm",
		"PUT",
		"/car",
		CarConfirm,
	},
	// -- just for me
	Route{
		"CarShow",
		"GET",
		"/cars/{carId}",
		CarShow,
	},
}