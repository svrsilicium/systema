# System A

The system A implements method GET:

* ***Get*** products' list
> It is a GET method that must be sent to URI ```http://localhost:8080/cars```. Returns JSON object with products.

Next methods are for intercommunication between systems, and not for users:

* ***Reserve*** product (from System B)
> It is a GET method (for now) that must be sent to URI ```http://localhost:8080/car/{carId}``` with an **id** of the product.
* ***Confirm*** product (from System B)
> It is a PUT method that must be sent to URI ```http://localhost:8080/car``` with a **hash** of the reservation in the request body.
* ***Refuse*** product (from System B). NOT IMPLEMENTED YET
> It should be a DELETE method sent to URI ```http://localhost:8080/car``` with a **hash** of reserved product to delete from reserved bulk and make it available again.

To test system A you can use your browser (URI: http://localhost:8080/cars), [Postman](https://www.getpostman.com/) or cURL.

bash cURL:
```
curl --request POST --url http://localhost:8080
```