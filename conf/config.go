package conf

// -- seconds, max reservation time
const ReservationTime int = 60

// -- seconds, timeout to check reservation time
const TimeToWait int = 1